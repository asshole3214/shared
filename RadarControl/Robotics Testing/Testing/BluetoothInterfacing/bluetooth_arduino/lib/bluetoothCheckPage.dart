import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:async';
import './BluetoothScanPage.dart';
import './customWidgets.dart';
import './MovementPage.dart';



class BluetoothCheckPage extends StatefulWidget {
  @override
  _BluetoothCheckPageState createState() => _BluetoothCheckPageState();
}

class _BluetoothCheckPageState extends State<BluetoothCheckPage> {
  
  BluetoothState _bluetoothState = BluetoothState.UNKNOWN;
  BluetoothConnection _connection ;
  TextEditingController bluetoothMACAddress;
  bool bluetoothState = false; // TODO: GET BLUETOOTH STATE HERE

  
 
  @override
  void btListener()async
  {
    _connection.input.listen((data)
    {
      int backspacesCounter = 0;
      data.forEach((byte) {
        if (byte == 8 || byte == 127) {
          backspacesCounter++;
        }
      });
      Uint8List buffer = Uint8List(data.length - backspacesCounter);
      int bufferIndex = buffer.length;

      // Apply backspace control character
      backspacesCounter = 0;
      for (int i = data.length - 1; i >= 0; i--) {
        if (data[i] == 8 || data[i] == 127) {
          backspacesCounter++;
        }
        else {
          if (backspacesCounter > 0) {
            backspacesCounter--;
          }
          else {
            buffer[--bufferIndex] = data[i];
          }
        }
      }

      // Create message if there is new line character
      //TODO: CREATE A VISUAL STATE FOR USER TO UNDERSTAND THE BLUETOOTH CONNECTION STATE
      String dataString = String.fromCharCodes(buffer);
      print(dataString);
      //TODO : CODE INTERPRATER CLASS FOR FLUTTER 
      _connection.close(); 
      print("connection to Bluetooth device closed");     
      //REDIRECT TO MOVMENT PAGE
      Navigator.push(context, new MaterialPageRoute(builder:(BuildContext context)=>new MovementPage()));
    });
  }
    
  @override
  void initState()
  { 
    super.initState();
    FlutterBluetoothSerial.instance.state.then((state){
      setState(() 
      {
        _bluetoothState = state;
        if(_bluetoothState == BluetoothState.STATE_ON)
        {  
          bluetoothState =true;
        }
        else
        {
            bluetoothState =false;
        }
      });
    });
  }
  

  @override
  void dispose() {
    setState(() {
          
        });
    super.dispose();
  }

  _btConn()async
  {
  
  }
  @override
  Widget build(BuildContext context) {

    Widget pingBoard()
    {
      return new RaisedButton
      (
        /// customWidgets not used here as function do not support async methods
        color:Colors.blue[300],
        child: new Container
        (
          width: 100.0,
          child: new Center
          (
            child:CustomWidgets.dashboard("Ping MegaPi board",fontSize: 12.0,color: Colors.grey[800]),
          )
        ),
        onPressed:()async
        {
          deviceMACAddress="00:1B:10:51:02:19";
          _connection = await BluetoothConnection.toAddress(deviceMACAddress);
          btListener(); // setting up listener event
          print("Opening connection to Bluetooth device");
          _connection.output.add(utf8.encode("Handshake" + "\r\n")); //handshake command given here
          Fluttertoast.showToast
          (
            msg: "Command sent awaiting response",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
          );
          await _connection.output.allSent.timeout(const Duration(seconds: 30),onTimeout:()
          {
            Fluttertoast.showToast
            (
              msg: "Error : Please reinitialise connection to megaPi",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0
            );
            print("Error : Please reinitialise connection to megaPi");
          }); //sending command
          print("Sent Command");
          
        },
      );
    }
    return Scaffold
    (
      body: new Center
      (
        child: Container
        (
          child:new Column
          (
            mainAxisAlignment:MainAxisAlignment.center,
            children: <Widget>
            [
              CustomWidgets.dashboard("Bluetooth Check",fontSize:30.0),
              new Padding(padding: EdgeInsets.all(20.0)),
              new Card
              (
                color: Colors.yellow[400],
                child: new Column
                (
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>
                  [
                    new ListTile // enbale bluetooth listile
                    (
                      leading:new Icon(Icons.bluetooth),
                      title:CustomWidgets.dashboard("Enable Bluetooth",fontSize: 20.0),
                      trailing: new Switch
                      (
                        activeColor: Colors.blue,
                        value:bluetoothState,
                        onChanged: (value) 
                        {
                          future() async { // async lambda seems to not working
                            if (value)
                            {
                                await FlutterBluetoothSerial.instance.requestEnable();
                                setState(() { bluetoothState = true;});
                            }
                            else
                            {
                              await FlutterBluetoothSerial.instance.requestDisable();
                               setState(() { bluetoothState = false;});
                            }
      
                          }
                          future().then((_) {
                            setState(() {});
                          });
                        }
                      ),
                    ),
                    //TODO : UPDATE BUTTON TO INVISIBLE WHEN BLUETOOTH IS OFF
                    //CustomWidgets.navButton(text:"Scan for MegaPI board",routingNav: (){Navigator.push(context, new MaterialPageRoute(builder:(BuildContext context)=>new BluetoothScanPage(connection: _connection,) ));}),
                    pingBoard(),
                    new Padding(padding:EdgeInsets.all(20.0)),
                  ],
                ),
              ),
            ],
          )
        ),
      )
    );
  }
}