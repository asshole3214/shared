import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:core';
import 'dart:async';
import './customWidgets.dart';
import './bluetoothCheckPage.dart';
import './BluetoothScanPage.dart';
import './interpration.dart';

 enum Options{SetSpeed,}

class MovementPage extends StatefulWidget {
  @override
  _MovementPageState createState() => _MovementPageState();
}

class _MovementPageState  extends State<MovementPage> {

  BluetoothConnection _connection ;
  History transmission = new History();
  DateTime timeSent ;
  final TextEditingController _controller = new TextEditingController(); 
 
  double _speedValue = 100, maxSpeed = 255;
  String movementState = "Idle";

  void btListener()async
  {
    _connection.input.listen((data)
    {
      int backspacesCounter = 0;
      data.forEach((byte) {
        if (byte == 8 || byte == 127) {
          backspacesCounter++;
        }
      });
      Uint8List buffer = Uint8List(data.length - backspacesCounter);
      int bufferIndex = buffer.length;

      // Apply backspace control character
      backspacesCounter = 0;
      for (int i = data.length - 1; i >= 0; i--) {
        if (data[i] == 8 || data[i] == 127) {
          backspacesCounter++;
        }
        else {
          if (backspacesCounter > 0) {
            backspacesCounter--;
          }
          else {
            buffer[--bufferIndex] = data[i];
          }
        }
      }
      print("connection to Bluetooth device closed");     

      String dataString = String.fromCharCodes(buffer);
      print(dataString);
      Duration _timelapsed;
      setState(() 
      {
         movementState =transmission.getMessage(dataString,timeSent);
         _timelapsed = transmission.timeLapsed;
         double _timeLapsedState=_timelapsed.inSeconds+(_timelapsed.inMilliseconds*0.0001);
         Fluttertoast.showToast
        (
          msg: "TimeLapased for recieved Message (in sec): ${_timeLapsedState.toStringAsFixed(3)}",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
        );
      });
      print(movementState);
    });
  }
  
  void initBTComms() async
  {
     if(deviceMACAddress != "")
    {
      _connection = await BluetoothConnection.toAddress(deviceMACAddress);  //Open connection
      btListener();
    }
    else
    {
      setState(() 
      {
        Fluttertoast.showToast
        (
          msg: "Error :Please reinitialise bluetooth device",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
        );
      });
    }
  }
  _movementBtnAction({String dir,double speedValue}) async
  {
    // writing to bluetooth device
    print("Movement Page : Forward pressed"); 
    if(deviceMACAddress !="")
    {
      print("Opening connection to Bluetooth device");
      _connection.output.add(utf8.encode("M:$dir,$speedValue" + "\r\n")); //handshake command given here
      await _connection.output.allSent; //sending command
      timeSent = new DateTime.now();
      print("Sent Command");
    }
    else
    {
      print("Error: please reinitialise bluetooth device");
      Fluttertoast.showToast
      (
        msg: "Error: please reinitialise bluetooth device",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
      );
    } 
  }

  @override
  void initState()
  { 
    super.initState();
    initBTComms();
  }
  @override
  void dispose() { 
    
    super.dispose();
    _connection.close();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold
    (
      appBar: new AppBar
      (
        backgroundColor: Colors.orange ,
        actions: <Widget>
        [
          new PopupMenuButton <Options>
          (
           
            onSelected: (Options results)
            {             
              switch(results)
              {
                case Options.SetSpeed:
                  showDialog
                  (
                    context: context,
                    builder:(context){
                      return Center
                      (
                        child: new Container
                        (
                          width: 350.0,
                          height: 336.0,
                          child: new Card
                          (
                            child:new Column
                            (
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>
                              [
                                new Padding(padding:new EdgeInsets.all(20.0) ,),
                                CustomWidgets.dashboard("Set Speed",fontSize: 30.0),
                                new Padding(padding:new EdgeInsets.all(20.0) ),
                                /*new SingleCircularSlider
                                (
                                  100,
                                  0,
                                  baseColor: Colors.orange,
                                  selectionColor: Colors.grey,
                                  onSelectionEnd:()
                                  {

                                  } ,
                                ),*/
                                CustomWidgets.dashboard("Current Speed: "+_speedValue.toString() ),
                                new Padding(padding:new EdgeInsets.all(20.0) ),
                                new TextField
                                (
                                  controller: _controller,
                                  decoration: InputDecoration
                                  (
                                    border: OutlineInputBorder(),
                                    labelText:"Set speed",
                                  ),
                                ),
                                new ButtonBar
                                (
                                  children: <Widget>
                                  [
                                    CustomWidgets.navButton(text:"Cancel",routingNav: (){Navigator.pop(context);}),
                                    CustomWidgets.navButton(text:"Set Speed",routingNav: ()
                                    {
                                      setState(() 
                                      {
                                        _speedValue = double.parse(_controller.text);
                                        _controller.clear();     
                                        Navigator.pop(context);
                                        print("Speed set to : $_speedValue");
                                        Fluttertoast.showToast
                                        (
                                          msg: "Speed set to : $_speedValue",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIos: 1,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white,
                                          fontSize: 16.0
                                        );
                                      
                                      });
                                    }),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }
                  );
                break;
              }
            },
            itemBuilder: (BuildContext context)=><PopupMenuEntry<Options>>
            [
              const PopupMenuItem<Options>(
                value: Options.SetSpeed,
                child: const Text('Set Speed'),
              ),
            ]
          )
        ],
      ),
      //TODO:change to slide up panel here
      //TODO:add in movement history onto the slide up page
      body: new Container
      (
        child: new Center
        (
          child: new SlidingUpPanel
          (
            body: new Container
            (
              child:new Column
              (
                mainAxisAlignment:MainAxisAlignment.center,
                children: <Widget>
                [
                CustomWidgets.dashboard("MovementState: "+movementState,fontSize: 30.0),
                //front end controls for megapi controls
                new Padding(padding: EdgeInsets.all(10.0),),
                CustomWidgets.movementButton(size: 125.0, color: Colors.green ,text: "Forward",onPressed:()=> _movementBtnAction(dir:"F",speedValue: _speedValue)),
                CustomWidgets.movementButton(size: 155.0, color: Colors.red ,text: "STOP",textColor: Colors.grey[800],onPressed:()=>_movementBtnAction(dir:"S",speedValue: 0 )), 
                CustomWidgets.movementButton(size: 125.0, color: Colors.yellow ,text: "Reverse",onPressed:()=>_movementBtnAction(dir:"B",speedValue: _speedValue )), 
                
                ],
              )
            ),
            renderPanelSheet: false,
            collapsed: new Container
            (
              child: new Row
              (
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>
                [
                  CustomWidgets.dashboard("Movment History"),
                ],
              )
            ),
            panel: new Container
            (
              decoration: new BoxDecoration
              (
                color: Colors.orange[200],
                borderRadius: new BorderRadius.only
                (
                  topLeft: const Radius.circular(40.0),
                  topRight: const Radius.circular(40.0)
                )
              ),
              child: new Center
              (
                child: new Column
                (
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>
                  [
                    //TODO:place list of MovmentHistory 
                  ],
                ),
              ),
            ),
          )
        )
      )
    );
  }
}