import 'dart:core';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';


//!   RULES FOR COMMUNICATIONS WITH SERIAL3 
//!   -> KEEP MESSAGE SHORT , (I.E MESSAGE < 1*SERIAL3.WRITE)
//!   -> INSERT THE KEYWORD "MEGAPI:" INFRONT OF WHATEVER MESSAGE YOU ARE SENDING 


List <History> transmissionHistory = new List();


class History
{
  
  DateTime timeSent;
  DateTime timeRecived;
  Duration timeLapsed;
  String originMessage;
  String pureMessage;
  History({this.timeRecived,this.timeSent,this.originMessage ,this.pureMessage,this.timeLapsed});


  String getMessage(String rawMessage, DateTime timeSent)
  {
    String originMessage = rawMessage.substring(1,7);
    String pureMessage = rawMessage.substring(7,rawMessage.length);
    DateTime timeOfMessage = new DateTime.now();
    this.timeLapsed = timeOfMessage.difference(timeSent);
    double timeLapsedState =  this.timeLapsed.inSeconds+(this.timeLapsed.inMilliseconds*0.0001);
    print("Time of recieved message : $timeOfMessage , Lapsed time (in sec):"+timeLapsedState.toStringAsFixed(3));
    transmissionHistory.add(new History(timeRecived:timeOfMessage,pureMessage: pureMessage,originMessage: originMessage,timeLapsed:this.timeLapsed));
    return pureMessage;
  }

}