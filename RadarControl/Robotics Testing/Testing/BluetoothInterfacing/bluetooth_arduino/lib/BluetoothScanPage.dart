import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:async';
import './bluetoothDeviceListEntry.dart';
import './customWidgets.dart';
import './bluetoothCheckPage.dart';

String deviceMACAddress="";
bool isConnected = false;


class BluetoothScanPage extends StatefulWidget {
  BluetoothConnection connection ;

  BluetoothScanPage({this.connection});
  @override
  _BluetoothScanPageState createState() => _BluetoothScanPageState();
}

class _BluetoothScanPageState extends State<BluetoothScanPage> {

  StreamSubscription<BluetoothDiscoveryResult> _streamSubscription;
  List<BluetoothDiscoveryResult> results = List<BluetoothDiscoveryResult>();
  BluetoothConnection connection ;
  bool isDiscovering = false;

  _BluetoothScanPageState({this.connection});
  @override
  void initState() { 
    super.initState();
    startDiscovery();
  }
  void startDiscovery()
  {
    setState(() {
      
      results.clear();
      isDiscovering = true;
    });
    _streamSubscription = FlutterBluetoothSerial.instance.startDiscovery().listen((r) {
    
      setState(()
        {  
          results.add(r);
          print(r);
        }
      );
    }); 
    _streamSubscription.onDone(() {
      setState(() { isDiscovering = false; });
    });
  }

   @override
   void dispose() {
    // Avoid memory leak (`setState` after dispose) and cancel discovery
    _streamSubscription?.cancel();
    connection.close();
    connection.dispose();

    super.dispose();
  }
  
  
  @override
  Widget build(BuildContext context) {

    Widget loading()
    {
      return new CircularProgressIndicator();
    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.yellow[400],
        title: isDiscovering ? Text('Discovering devices') : Text('Discovered devices'),
        actions: <Widget>[
          (
            isDiscovering ?
              FittedBox(child: Container(
                margin: new EdgeInsets.all(16.0),
                child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.white))
              ))
            :
              IconButton(
                icon: Icon(Icons.replay),
                onPressed:()
                {
                  // call for discovery code here
                  startDiscovery();
                  setState(() {isDiscovering = true;});
                }
              )
          )
        ],
      ),
      body:ListView.builder(
        itemCount: results.length,
        itemBuilder: (BuildContext context, index) {
          BluetoothDiscoveryResult result = results[index];
          return BluetoothDeviceListEntry(
            device: result.device,
            rssi: result.rssi,
            onTap: () async {
              
              deviceMACAddress = result.device.address;
              print('captured ${result.device.name} MAC address ${deviceMACAddress}.');
              Fluttertoast.showToast
              (
                msg: 'captured ${result.device.name} MAC address ${deviceMACAddress}.',
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0
              );
              
              //isConnected = bonded;
              Navigator.of(context).pop(result.device);
            },
            onLongPress: () async {
              try {
                //BONDING PROCESSS
                bool bonded = false;
                if (result.device.isBonded) {
                  print('Unbonding from ${result.device.address}...');
                  Fluttertoast.showToast
                  (
                    msg: 'Unbonding from ${result.device.address}...',
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIos: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0
                  );
                  await FlutterBluetoothSerial.instance.removeDeviceBondWithAddress(result.device.address);
                  print('Unbonding from ${result.device.address} has succed');
                  Fluttertoast.showToast
                  (
                    msg: 'Unbonding from ${result.device.address} has succed',
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIos: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0
                  );
                  
                  isConnected = false ; 
                }
                else {
                  print('Bonding with ${result.device.address}...');
                  Fluttertoast.showToast
                  (
                    msg: 'Bonding with ${result.device.address}...',
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIos: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0
                  );
                  bonded = await FlutterBluetoothSerial.instance.bondDeviceAtAddress(result.device.address);
                  deviceMACAddress = result.device.address;
                 
                  print('Bonding with ${result.device.address} has ${bonded ? 'succed' : 'failed'}.');
                  Fluttertoast.showToast
                  (
                    msg: 'Bonding with ${result.device.address} has ${bonded ? 'succed' : 'failed'}.',
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIos: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0
                  );

                  isConnected  = bonded;
                  Navigator.pop(context);
                }
                setState(() {
                  results[results.indexOf(result)] = BluetoothDiscoveryResult(
                    device: BluetoothDevice(
                      name: result.device.name ?? '',
                      address: result.device.address,
                      type: result.device.type,
                      bondState: bonded ? BluetoothBondState.bonded : BluetoothBondState.none,
                    ), 
                    rssi: result.rssi
                  );
                });
              }
              catch (ex) {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text('Error occured while bonding'),
                      content: Text("${ex.toString()}"),
                      actions: <Widget>[
                        new FlatButton(
                          child: new Text("Close"),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );
              }
            }
          );
        },
      )
    );
  }
}