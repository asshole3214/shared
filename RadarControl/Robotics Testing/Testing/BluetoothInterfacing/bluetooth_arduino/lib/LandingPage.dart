import 'package:flutter/material.dart';
import './bluetoothCheckPage.dart';
import './MovementPage.dart';
import './customWidgets.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {


  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: new Center
      (
        child: new Column
        (
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>
          [
            CustomWidgets.dashboard("MegaPi Control Dashboard",fontSize: 30.0),
            new Padding(padding: EdgeInsets.all(20.0)),
            CustomWidgets.navButton(text:"Bluetooth check",routingDestination: "Navigating : BluetoothCheckPage",routingNav:(){ Navigator.push(context, new MaterialPageRoute(builder:(BuildContext context)=>new BluetoothCheckPage()));} ),
            new Padding(padding: EdgeInsets.all(10.0)),
            CustomWidgets.navButton(text:"Movement",routingDestination: "Navigating : MovementPage",routingNav: (){ Navigator.push(context, new MaterialPageRoute(builder:(BuildContext context)=>new MovementPage()));}),

          ],
        ),
      ),
    );  
  }
}