import 'package:flutter/material.dart';

class CustomWidgets
{
  
  static Widget dashboard(String text , {double fontSize,Color color})
  {
    return new Text
    (
      text,
      style: new TextStyle
      (
        color: color == null?
        Colors.grey[600]
        :color,
        fontWeight: FontWeight.bold , 
        fontSize: fontSize,
      ),
    );
  }
 static Widget navButton({String text, String routingDestination,@required void routingNav()})
  {
    return new RaisedButton
    (

      color:Colors.blue[300],
      child: new Container
      (
        width: 100.0,
        child: new Center
        (
          child: dashboard(text,fontSize: 12.0,color: Colors.grey[800]),
        )
        
      ),
      onPressed: ()
      {
        //TODO: ADD NAV ROUTING HERE
        print(routingDestination);
        routingNav();
      },
    );
  }

  static Widget movementButton({double size,double fontSize, Color color,Color textColor,String text,void onPressed()})
  {
    return new Container
    (
      height: size, 
      width: size,

      child: new RaisedButton
      (
        // TODO: use custom widgets intergration 
        color: color, //TODO: use feed in data 
        child: dashboard(text, fontSize: fontSize == null ? 20.0 : fontSize ,color: textColor),
        onPressed:() async 
        {
          await onPressed();
        }
      ),
    );
  }
  static Widget bluetoothManual({TextEditingController bluetoothMACAddress,routing })
  {
    return new ListTile
    (
      trailing: new Icon(Icons.bluetooth),
      title:  dashboard("Bluetooth Manual input",fontSize: 10.0),
      subtitle: new Column
      (
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>
        [
          new TextField(controller: bluetoothMACAddress, ),
          navButton(text:"Enter",routingNav:routing),
        
        ],
      )
    );
  }
}